<?php
/**
 * @file
 * Contains UpdateDeployCommand class
 */

/**
 * Update command method descriptor.
 */
class UpdateDeployCommand {
  /**
   * Method name of command
   * @var string
   */
  protected $name;

  /**
   * Description from method docblock
   * @var string
   */
  protected $description;

  /**
   * Can command be reverted?
   * @var bool
   */
  protected $revertible;

  /**
   * Public constructor.
   * 
   * @param ReflectionMethod $method
   *   Reflection of command method
   */
  public function __construct(ReflectionMethod $method) {
    $this->name = $method->name;
    $this->description = _update_deploy_clean_comment($method->getDocComment());
    $this->revertible = method_exists($method->class, $method->name . 'Revert');
  }

  /**
   * Get command name.
   * 
   * @return string
   *   Name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get human readable command title.
   * 
   * @return string
   *   Command title
   */
  public function getTitle() {
    return _update_deploy_format_camel_case_to_human($this->name);
  }

  /**
   * Get command description from method docblock.
   * 
   * @return string
   *   Description
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Check if command has an associated revert method.
   *
   * @return bool
   *   Can command be reverted?
   */
  public function isRevertible() {
    return $this->revertible;
  }
}
