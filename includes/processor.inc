<?php
/**
 * @file
 * Contains the UpdateDeployProcessor class
 */

/**
 * Update processor.
 */
class UpdateDeployProcessor {

  protected $reflection;
  protected $name;
  protected $description;
  protected $filename;
  protected $commands = array();
  protected $statusCode;
  protected $statusTime;
  protected $statusMessage;
  protected $errors = array();

  /**
   * Update has never been applied.
   */
  const STATUS_NONE = 0;

  /**
   * Update has been successfully applied and subsequently reverted.
   */
  const STATUS_REVERTED = 1;

  /**
   * Update failed to apply.
   */
  const STATUS_FAILED = 2;

  /**
   * Update has been successfully applied.
   */
  const STATUS_COMPLETE = 3;

  /**
   * Update has never been applied and has been flagged to be skipped.
   */
  const STATUS_SKIPPED = 4;

  /**
   * Public constructor.
   *
   * @param ReflectionClass $reflection
   *   Reflection of update class
   */
  public function __construct(ReflectionClass $reflection) {
    global $base_path;

    $this->reflection = $reflection;
    $this->name = $reflection->name;
    $this->description = _update_deploy_clean_comment($reflection->getDocComment());
    $this->filename = str_replace(DRUPAL_ROOT . $base_path, '', $reflection->getFileName());

    $status = update_deploy_status_load($this->name);
    $this->statusCode = $status->status;
    $this->statusTime = $status->time;
    $this->statusMessage = $status->message;

    foreach ($reflection->getMethods() as $method) {
      if ($method->isPublic() && !preg_match('/(Revert)$/', $method->name)) {
        $this->commands[$method->name] = new UpdateDeployCommand($method);
      }
    }
  }

  /**
   * Get class name of update.
   *
   * @return string
   *   Class name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get human readable title of update.
   *
   * @return string
   *   Title
   */
  public function getTitle() {
    return _update_deploy_format_camel_case_to_human($this->name);
  }

  /**
   * Get update class description from docblock.
   *
   * @return string
   *   Description
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Get filename of class.
   *
   * @return string
   *   Filename
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Get list of commands contained in update.
   *
   * @return array
   *   Array of commands
   */
  public function getCommands() {
    return $this->commands;
  }

  /**
   * Get command by name.
   *
   * @param string $name
   *   Command name
   *
   * @return UpdateDeployCommand
   *   Named command
   */
  public function getCommand($name) {
    return isset($this->commands[$name]) ? $this->commands[$name] : NULL;
  }

  /**
   * Get status code.
   *
   * @return int
   *   Status code
   */
  public function getStatusCode() {
    return $this->statusCode;
  }

  /**
   * Get time of last status change.
   *
   * @return int
   *   Status timestamp
   */
  public function getStatusTime() {
    return $this->statusTime;
  }

  /**
   * Get any message e.g. error associated with the status.
   *
   * @return string
   *   Status message
   */
  public function getStatusMessage() {
    return $this->statusMessage;
  }

  /**
   * Check if update is pending.
   *
   * @return bool
   *   Is update pending?
   */
  public function isPending() {
    return $this->getStatusCode() < self::STATUS_COMPLETE;
  }

  /**
   * Check if update is complete.
   *
   * @return bool
   *   Is update complete?
   */
  public function isComplete() {
    return $this->getStatusCode() == self::STATUS_COMPLETE;
  }

  /**
   * Check if all commands in this update can be reverted.
   *
   * @return bool
   *   Can update be reverted?
   */
  public function isFullyRevertible() {
    foreach ($this->commands as $command) {
      if (!$command->isRevertible()) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Check if any commands in this update can be reverted.
   *
   * @return bool
   *   Can update be reverted?
   */
  public function isRevertible() {
    foreach ($this->commands as $command) {
      if ($command->isRevertible()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get human readable label for status code.
   *
   * @return string
   *   Label text
   */
  public function getStatusName() {
    $labels = array(
      self::STATUS_NONE     => t('Pending'),
      self::STATUS_REVERTED => t('Reverted'),
      self::STATUS_FAILED   => t('Failed'),
      self::STATUS_COMPLETE => t('Complete'),
      self::STATUS_SKIPPED  => t('Skipped'),
    );
    return isset($labels[$this->statusCode]) ? $labels[$this->statusCode] : '';
  }

  /**
   * Set status of update.
   *
   * @param int $status_code
   *   New status code
   * @param string $message
   *   Message associated with status e.g. error on fail
   */
  protected function setStatus($status_code, $message = '') {
    $this->statusTime = time();
    $this->statusCode = $status_code;
    $this->statusMessage = $message;

    update_deploy_status_save(array(
      'class_name'  => $this->name,
      'time'        => $this->statusTime,
      'status'      => $this->statusCode,
      'message'     => $this->statusMessage,
    ));
  }

  /**
   * Get instance of update class.
   *
   * @return UpdateDeployUpdate
   *   Update instance
   */
  protected function getInstance() {
    $class_name = $this->name;
    return new $class_name();
  }

  /**
   * Get update class source code.
   *
   * @return string
   *   Source code
   */
  public function getSource() {
    $filename = $this->reflection->getFileName();
    $start_line = $this->reflection->getStartLine() - 1;
    $end_line = $this->reflection->getEndLine();
    $length = $end_line - $start_line;

    $source = file($filename);
    return implode('', array_slice($source, $start_line, $length));
  }

  /**
   * Apply the update.
   *
   * @return bool
   *   Whether update ran successfully
   */
  public function apply() {
    watchdog('update_deploy', 'Attempting to apply update @class', array('@class' => $this->getName()), WATCHDOG_INFO);
    $this->errors = array();

    $transaction = db_transaction();
    $instance = $this->getInstance();

    $completed = array();

    foreach ($this->commands as $command) {
      $method = $command->getName();

      try {
        if ($instance->$method() == TRUE) {
          array_unshift($completed, $command);
        }
        else {
          throw new UpdateDeployCommandFailException();
        }
      }
      catch (Exception $e) {
        $transaction->rollback();

        $message_params = array(
          '@class' => $this->getName(),
          '@method' => $method,
          '@message' => $e->getMessage(),
        );
        $error = t('@class::@method() failed to apply: @message', $message_params);
        $this->errors[] = $error;
        watchdog('update_deploy', '@class::@method() failed to apply: @message', $message_params, WATCHDOG_ERROR);

        $this->setStatus(self::STATUS_FAILED, $error);

        // Attempt to revert any completed commands.
        foreach ($completed as $command) {
          if ($command->isRevertible()) {
            $revert_method = $command->getName() . 'Revert';
            try {
              $instance->$revert_method();
            }
            catch (Exception $e) {
              $message_params = array(
                '@class' => $this->getName(),
                '@method' => $command->getName(),
                '@message' => $e->getMessage(),
              );
              $this->errors[] = t('@class::@method() failed to revert: @message', $message_params);
              watchdog('update_deploy', '@class::@method() failed to revert: @message', $message_params, WATCHDOG_ERROR);
            }
          }
        }

        return FALSE;
      }
    }

    $this->setStatus(self::STATUS_COMPLETE);
    watchdog('update_deploy', 'Update @class applied successfully', array('@class' => $this->getName()), WATCHDOG_INFO);

    return TRUE;
  }

  /**
   * Revert update.
   */
  public function revert() {
    $this->errors = array();
    $commands = array();
    $instance = $this->getInstance();

    foreach ($this->commands as $command) {
      if ($command->isRevertible()) {
        array_unshift($commands, $command);
      }
    }

    foreach ($commands as $command) {
      $revert_method = $command->getName() . 'Revert';
      try {
        $instance->$revert_method();
      }
      catch (Exception $e) {
        $message_params = array(
          '@class' => $this->getName(),
          '@method' => $command->getName(),
          '@message' => $e->getMessage(),
        );
        $this->errors[] = t('@class::@method() failed to revert: @message', $message_params);
        watchdog('update_deploy', '@class::@method() failed to revert: @message', $message_params, WATCHDOG_ERROR);
      }
    }

    $this->setStatus(self::STATUS_REVERTED);

    if (empty($this->errors)) {
      watchdog('update_deploy', 'Update @class was reverted', array('@class' => $this->getName()), WATCHDOG_INFO);
      return TRUE;
    }
    else {
      watchdog('update_deploy', 'Update @class was reverted but @count of @total revert commands failed', array(
        '@class' => $this->getName(),
        '@count' => count($this->errors),
        '@total' => count($commands),
      ), WATCHDOG_INFO);
      return FALSE;
    }
  }

  /**
   * Skip this update.
   */
  public function skip() {
    $this->errors = array();
    $this->setStatus(self::STATUS_SKIPPED);
    watchdog('update_deploy', 'Update @class was skipped', array('@class' => $this->getName()), WATCHDOG_INFO);
    return TRUE;
  }

  /**
   * Get error list from last operation.
   *
   * @return array
   *   Error list
   */
  public function getErrors() {
    return $this->errors;
  }
}
