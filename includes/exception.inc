<?php
/**
 * @file
 * Includes UpdateCommandFailException class.
 */

/**
 * Update exception.
 */
class UpdateDeployCommandFailException extends Exception {
  /**
   * Public constructor.
   */
  public function __construct($message = NULL) {
    if (!$message) {
      $message = t('Method returned FALSE.');
    }
    parent::__construct($message);
  }
}
