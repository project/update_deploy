<?php
/**
 * @file
 * Admin page callbacks.
 */

/**
 * Apply update form builder.
 */
function update_deploy_apply_pending_form($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {

    $pending = update_deploy_processor_load_pending();
    if (!empty($pending)) {
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Apply all pending updates'),
      );
    }

    $classes = array(
      UpdateDeployProcessor::STATUS_NONE => 'warning',
      UpdateDeployProcessor::STATUS_REVERTED => 'warning',
      UpdateDeployProcessor::STATUS_FAILED => 'error',
      UpdateDeployProcessor::STATUS_COMPLETE => 'ok',
      UpdateDeployProcessor::STATUS_SKIPPED => 'info',
    );

    $rows = array();
    foreach (update_deploy_processor_load_all() as $processor) {
      $processor_path = 'admin/structure/update_deploy/' . $processor->getName();
      $query = array('destination' => 'admin/structure/update_deploy');

      // Build operations link list.
      $links = array();
      if (user_access('apply update_deploy updates')) {
        $links[] = array(
          'title' => update_deploy_apply_title($processor),
          'href' => $processor_path . '/apply',
          'query' => $query,
        );
      }
      if (update_deploy_revert_access($processor)) {
        $links[] = array(
          'title' => t('Revert'),
          'href' => $processor_path . '/revert',
          'query' => $query,
        );
      }
      if (update_deploy_skip_access($processor)) {
        $links[] = array(
          'title' => t('Skip'),
          'href' => $processor_path . '/skip',
          'query' => $query,
        );
      }
      if (user_access('view update_deploy updates source')) {
        $links[] = array(
          'title' => t('View source'),
          'href' => $processor_path . '/source',
          'query' => $query,
        );
      }

      $operations = theme('links', array(
        'links' => $links,
        'attributes' => array('class' => array('inline')),
      ));

      $row = array(
        'data' => array(
          l($processor->getTitle(), $processor_path) .
            '<div class="description">' . check_plain($processor->getDescription()) . '</div>',
          check_plain($processor->getStatusName()),
          $processor->getStatusTime() ? format_date($processor->getStatusTime()) : t('n/a'),
          $operations,
        ),
        'class' => array($classes[$processor->getStatusCode()]),
      );
      $rows[] = $row;
    }

    $header = array(
      t('Name'),
      t('Status'),
      t('Time'),
      t('Operations'),
    );

    $form['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => array_reverse($rows),
      '#empty' => t('No UpdateDeployUpdate classes have been detected. See README.txt.'),
    );

    return $form;
  }
  else {
    return confirm_form($form, t('Apply all pending updates?'), 'admin/structure/update_deploy');
  }
}

/**
 * Apply update submit handler.
 */
function update_deploy_apply_pending_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    foreach (update_deploy_processor_load_pending() as $processor) {
      if (!$processor->apply()) {
        drupal_set_message(t('Update @class failed to apply.', array(
          '@class' => $processor->getName(),
        )), 'error');
        foreach ($processor->getErrors() as $error) {
          drupal_set_message($error, 'error');
        }
        return;
      }
    }
    drupal_set_message(t('All update were successfully applied.'));
  }
}

/**
 * Update title callback.
 */
function update_deploy_processor_title($processor) {
  return $processor->getTitle();
}

/**
 * Form builder to view update and display actions.
 */
function update_deploy_processor_view_form($form, &$form_state, $processor) {
  $form_state['processor'] = $processor;

  if ($processor->getDescription()) {
    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'item',
      '#markup' => check_plain($processor->getDescription()),
    );
  }

  $form['filename'] = array(
    '#title' => t('Filename'),
    '#type' => 'item',
    '#markup' => check_plain($processor->getFilename()),
  );

  $status = $processor->getStatusName();
  $time = $processor->getStatusTime();

  if ($time) {
    $status .= ' (' . format_date($time) .')';
  }

  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'item',
    '#markup' => $status,
  );

  if ($processor->getStatusMessage()) {
    $form['message'] = array(
      '#type' => 'item',
      '#markup' => '<div class="error">' . check_plain($processor->getStatusMessage()) . '</div>',
    );
  }

  $rows = array();
  foreach ($processor->getCommands() as $command) {
    $rows[] = array(
      'class' => $command->isRevertible() ? array() : array('warning'),
      'data' => array(
        check_plain($command->getTitle()) .
          '<div class="description">' . check_plain($command->getDescription()) . '</div>',
        $command->isRevertible() ? t('Yes') : t('No'),
      ),
    );
  }

  $form['commands'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => array(t('Command'), t('Is revertible?'))
  );

  $form['actions'] = array();

  if (user_access('apply update_deploy updates')) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => update_deploy_apply_title($processor),
    );
  }
  if (update_deploy_skip_access($processor)) {
    $form['actions']['skip'] = array(
      '#type' => 'submit',
      '#value' => t('Skip'),
    );
  }
  if (update_deploy_revert_access($processor)) {
    $form['actions']['revert'] = array(
      '#type' => 'submit',
      '#value' => t('Revert'),
    );
  }

  return $form;
}

/**
 * Submit handler for update_deploy_processor_view_form.
 */
function update_deploy_processor_view_form_submit($form, &$form_state) {
  $processor = $form_state['processor'];
  $path = 'admin/structure/update_deploy/' . $processor->getName();

  switch ($form_state['values']['op']) {
    case update_deploy_apply_title($processor):
      $form_state['redirect'] = $path . '/apply';
      break;
    case t('Skip'):
      $form_state['redirect'] = $path . '/skip';
      break;
    case t('Revert'):
      $form_state['redirect'] = $path . '/revert';
      break;
  }
}

/**
 * Page callback to view update class source.
 */
function update_deploy_processor_source($processor) {
  return array(
    '#theme' => 'update_deploy_source',
    '#source' => $processor->getSource(),
  );
}

/**
 * Form builder for apply update.
 */
function update_deploy_processor_apply_form($form, &$form_state, $processor) {
  $form_state['processor'] = $processor;

  switch ($processor->getStatusCode()) {
    case UpdateDeployProcessor::STATUS_COMPLETE:
      $question = t('Reapply update %title?', array('%title' => $processor->getTitle()));
      $description = t('Warning: this update has already been applied. Only do this if you’re sure you know what you’re doing.');
      break;
    case UpdateDeployProcessor::STATUS_REVERTED:
      $question = t('Reapply update %title?', array('%title' => $processor->getTitle()));
      $description = t('This update has previously been applied and reverted. Apply it again?');
      break;
    case UpdateDeployProcessor::STATUS_FAILED:
      $question = t('Retry update %title?', array('%title' => $processor->getTitle()));
      $description = t('This update failed on the last attempt. Do you want to try again?');
      break;
    default:
      $question = t('Apply update %title?', array('%title' => $processor->getTitle()));
      $description = t('Are you sure you want to apply this update?');
  }

  if (!$processor->isFullyRevertible()) {
    if (!$processor->isRevertible()) {
      $description .= ' ' . t('It cannot be reverted.');
    }
    else {
      $description .= ' ' . t('It cannot be fully reverted.');
    }
  }

  return confirm_form($form, $question, 'admin/structure/update_deploy/' . $processor->getName(), $description);
}

/**
 * Submit handler for apply update.
 */
function update_deploy_processor_apply_form_submit($form, &$form_state) {
  $processor = $form_state['processor'];

  if ($processor->apply()) {
    drupal_set_message(t('Update %title was applied successfully', array('%title' => $processor->getTitle())));
  }
  else {
    drupal_set_message(t('Update %title failed to apply', array('%title' => $processor->getTitle())), 'error');
    foreach ($processor->getErrors() as $error) {
      drupal_set_message($error, 'error');
    }
  }
  $form_state['redirect'] = 'admin/structure/update_deploy/' . $processor->getName();
}

/**
 * Form builder for skip update.
 */
function update_deploy_processor_skip_form($form, &$form_state, $processor) {
  $form_state['processor'] = $processor;
  $question = t('Skip update %title?', array('%title' => $processor->getTitle()));
  $description = t('It will no longer be classed as pending, but can still be manually applied.');
  $path = 'admin/structure/update_deploy/' . $processor->getName();
  return confirm_form($form, $question, $path, $description);
}

/**
 * Submit handler for skip update.
 */
function update_deploy_processor_skip_form_submit($form, &$form_state) {
  $processor = $form_state['processor'];
  $processor->skip();
  drupal_set_message(t('Update %title will now be skipped.', array('%title' => $processor->getTitle())));
  $form_state['redirect'] = 'admin/structure/update_deploy/' . $processor->getName();
}


/**
 * Form builder for skip update.
 */
function update_deploy_processor_revert_form($form, &$form_state, $processor) {
  $form_state['processor'] = $processor;
  $question = t('Revert update %title?', array('%title' => $processor->getTitle()));
  $description = t('Are you sure you want to revert this update?');
  $path = 'admin/structure/update_deploy/' . $processor->getName();

  if (!$processor->isFullyRevertible()) {
    $description .= ' ' . t('Not all commands can be reverted.');
  }

  return confirm_form($form, $question, $path, $description);
}

/**
 * Submit handler for revert update.
 */
function update_deploy_processor_revert_form_submit($form, &$form_state) {
  $processor = $form_state['processor'];
  if ($processor->revert()) {
    drupal_set_message(t('Update %title was reverted.', array('%title' => $processor->getTitle())));
  }
  else {
    drupal_set_message(t('Update %title failed to revert', array('%title' => $processor->getTitle())), 'error');
    foreach ($processor->getErrors() as $error) {
      drupal_set_message($error, 'error');
    }
  }
  $form_state['redirect'] = 'admin/structure/update_deploy/' . $processor->getName();
}
