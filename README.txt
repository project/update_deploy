# What is update_deploy?

This module achieves nothing by iteself. You need to write code in your own
UpdateDeployUpdate classes in order to deploy changes.

Many Drupal developers were using hook_update_n in a custom module to deploy
changes, update_deploy comes with a very helpful utlity api and a much better
developer experience for deploying changes in a consistant, repeatable manner.


Some advantages of update_deploy over update hooks include:

 - Admin UI.
 - Able to re-run updates.
 - Able to re-run updates.
 - @TODO Complete this.

# Example usage.

Your UpdateDeployUpdate classes need to be placed in the following locations.

sites/all/updates/
sites/example.com/updates/

Sub-directories of these locations work as well e.g.

sites/example.com/updates/sprint-005/

The convention is to place a single UpdateDeployUpdate class in single file, however
this isn't mandatory.

Each file containing a UpdateDeployUpdate class must end in .inc in order for update_deploy
to detect the class declaration.

# Example update classes.

* Disable, enable, uninstall modules.
* variable_set().
* block config.
