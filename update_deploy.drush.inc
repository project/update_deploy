<?php
/**
 * @file
 * Drush Commands.
 */

/**
 * Implements hook_drush_command().
 */
function update_deploy_drush_command() {
  $items['update-deploy-info'] = array(
    'description' => "Show status of update.",
    'aliases' => array('udi'),
  );

  $items['update-deploy-apply-pending'] = array(
    'description' => "Apply all pending updates.",
    'aliases' => array('udap'),
  );

  $items['update-deploy-apply'] = array(
    'description' => "Apply an individual update.",
    'examples' => array(
      'drush update-deploy-apply MyUpdate' => 'Apply update class MyUpdate.',
    ),
    'aliases' => array('uda'),
    'arguments' => array(
      'class' => t('Class name of update'),
    ),
  );

  $items['update-deploy-skip'] = array(
    'description' => "Skip an update.",
    'examples' => array(
      'drush update-deploy-skip MyUpdate' => 'Skip update class MyUpdate.',
    ),
    'aliases' => array('uds'),
    'arguments' => array(
      'class' => t('Class name of update'),
    ),
  );

  return $items;
}

/**
 * Drush command to show status of update.
 */
function drush_update_deploy_info() {
  foreach (update_deploy_processor_load_all() as $processor) {
    print dt('@class: @status @time', array(
      '@class' => $processor->getName(),
      '@status' => $processor->getStatusName(),
      '@time' => $processor->getStatusTime() ? '(' . format_date($processor->getStatusTime()) . ')' : '',
    )) . "\n";
  }
}

/**
 * Drush command to apply all pending updates.
 */
function drush_update_deploy_apply_pending() {
  $processors = update_deploy_processor_load_pending();

  if (empty($processors)) {
    drush_log(dt('No pending updates.'), 'success');
    return;
  }

  // Todo: plural.
  if (!drush_confirm(dt('Are you sure you want to apply @count updates?', array(
        '@count' => count($processors),
      )))) {
    return;
  }

  foreach ($processors as $processor) {
    // Stop if any update fails.
    if (!$processor->apply()) {
      drush_log(dt('Update @class failed to apply.', array(
        '@class' => $processor->getName(),
      )), 'error');
      return;
    }
    drush_log(dt('Update @class successfully applied.', array(
      '@class' => $processor->getName(),
    )), 'success');
  }
  drush_log(dt('All updates were successfully applied.'), 'success');
}

/**
 * Drush command to apply a specific update.
 */
function drush_update_deploy_apply($class_name) {
  $processor = update_deploy_processor_load($class_name);
  if (!$processor) {
    drush_log(dt('Couldn’t find update @class.', array(
      '@class' => $class_name,
    )), 'error');
  }
  if (!drush_confirm(dt('Are you sure you want to apply this update?'))) {
    return;
  }
  if ($processor->apply()) {
    drush_log(dt('Update was successfully applied.'), 'success');
  }
  else {
    drush_log(dt('Update failed to apply.'), 'error');
  }
}

/**
 * Drush command to skip a specific update.
 */
function drush_update_deploy_processor_skip($class_name) {
  $processor = update_deploy_processor_load($class_name);
  if (!$processor) {
    drush_log(dt('Couldn’t find update @class.', array(
      '@class' => $class_name,
    )), 'error');
  }
  if (!drush_confirm(dt('Are you sure you want to skip this update?'))) {
    return;
  }
  $processor->skip();
  drush_log(dt('Update will now be skipped.'), 'success');
}
