<?php
/**
 * @file
 * Unsuccessful revert.
 */

/**
 * Unsuccessful revert.
 */
class UnsuccessfulRevert extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand1() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should run successfully.
   */
  public function successfulCommand2() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should run successfully.
   */
  public function successfulCommand3() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should be called on fail.
   */
  public function successfulCommand1Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
  }

  /**
   * Should be called on fail.
   */
  public function successfulCommand2Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    throw new Exception('Error message');
  }

  /**
   * Should be called on fail.
   */
  public function successfulCommand3Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
  }
}
