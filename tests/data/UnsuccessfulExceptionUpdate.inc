<?php
/**
 * @file
 * Unsuccessful update with exception.
 */

/**
 * Unsuccessful update with exception.
 */
class UnsuccessfulExceptionUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand1() {
    return TRUE;
  }

  /**
   * Should fail to run.
   */
  public function failCommand() {
    throw new Exception('Error message');
  }
}
