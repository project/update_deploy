<?php
/**
 * @file
 * Reverted update.
 */

/**
 * Reverted update.
 */
class RevertedUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand() {
    return TRUE;
  }
}
