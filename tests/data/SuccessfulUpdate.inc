<?php
/**
 * @file
 * Successful update.
 */

/**
 * Successful update.
 */
class SuccessfulUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand1() {
    return TRUE;
  }

  /**
   * Should run successfully.
   */
  public function successfulCommand2() {
    return TRUE;
  }
}
