<?php
/**
 * @file
 * Unsuccessful update with unsuccessful revert.
 */

/**
 * Unsuccessful update with unsuccessful revert.
 */
class UnsuccessfulUpdateWithUnsuccessfulRevert extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand1() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should run successfully.
   */
  public function successfulCommand2() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should fail to run.
   */
  public function failCommand() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return FALSE;
  }

  /**
   * Should never be called.
   */
  public function successfulCommand3() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    return TRUE;
  }

  /**
   * Should be called to revert first command on fail.
   */
  public function successfulCommand1Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
    throw new Exception('Error message');
  }

  /**
   * Should be called to revert first command on fail.
   */
  public function successfulCommand2Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
  }

  /**
   * Should never be called.
   */
  public function failCommandRevert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
  }

  /**
   * Should never be called.
   */
  public function successfulCommand3Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = __FUNCTION__;
  }
}
