<?php
/**
 * @file
 * Complete update.
 */

/**
 * Complete update.
 */
class CompleteUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand() {
    return TRUE;
  }
}
