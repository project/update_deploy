<?php
/**
 * @file
 * Partially Revertible update.
 */

/**
 * Partially Revertible update.
 */
class PartiallyRevertibleUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function revertibleCommand() {
    return TRUE;
  }

  /**
   * Reverts revertibleCommand().
   */
  public function revertibleCommandRevert() {
  }

  /**
   * Should run successfully.
   */
  public function unrevertibleCommand() {
    return TRUE;
  }
}
