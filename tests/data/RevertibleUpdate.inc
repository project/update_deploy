<?php
/**
 * @file
 * Revertible update.
 */

/**
 * Revertible update.
 */
class RevertibleUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand1() {
    return TRUE;
  }

  /**
   * Reverts successfulCommand1.
   */
  public function successfulCommand1Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = 'successfulCommand1Revert';
  }

  /**
   * Should run successfully.
   */
  public function successfulCommand2() {
    return TRUE;
  }

  /**
   * Reverts successfulCommand2.
   */
  public function successfulCommand2Revert() {
    global $_update_deploy_methods_called;
    $_update_deploy_methods_called[] = 'successfulCommand2Revert';
  }
}
