<?php
/**
 * @file
 * Failed update.
 */

/**
 * Failed update.
 */
class FailedUpdate extends UpdateDeployUpdate {
  /**
   * Should run successfully.
   */
  public function successfulCommand() {
    return TRUE;
  }
}
